package pl.rybak.stringaclculator


import spock.lang.Shared
import spock.lang.Specification

class StringCalculatorSpec extends Specification {

    @Shared
    def calculator

    void setup() {
        calculator = new StringCalculator()
    }

    def "add should properly sums 0, 1 or 2 numbers with ',' delimiter"() {
        expect:
        calculator.add(values) == expected

        where:
        values | expected
        ""     | 0
        "1"    | 1
        "1,2"  | 3
    }

    def "add should properly sums n numbers with ',' delimiter"() {
        expect:
        calculator.add(values) == expected

        where:
        values            | expected
        ""                | 0
        "1"               | 1
        "1,2"             | 3
        "1,2,3"           | 6
        "1,2,3,4"         | 10
        "1,2,3,4,5"       | 15
        "1,2,3,4,5,6"     | 21
        "1,2,3,4,5,6,7"   | 28
        "1,2,3,4,5,6,7,8" | 36
    }

    def "add should properly sums n numbers with '\n' delimiter"() {
        expect:
        calculator.add(values) == expected

        where:
        values                   | expected
        ""                       | 0
        "1"                      | 1
        "1\n2"                   | 3
        "1\n2\n3"                | 6
        "1\n2\n3\n4"             | 10
        "1\n2\n3\n4\n5"          | 15
        "1\n2\n3\n4\n5\n6"       | 21
        "1\n2\n3\n4\n5\n6\n7"    | 28
        "1\n2\n3\n4\n5\n6\n7\n8" | 36
    }

    def "add should properly sums n numbers with delimiters '\n' and ',' delimiter"() {
        expect:
        calculator.add(values) == expected

        where:
        values                 | expected
        ""                     | 0
        "1"                    | 1
        "1,2"                  | 3
        "1\n2,3"               | 6
        "1\n2\n3\n4"           | 10
        "1\n2\n3\n4\n5"        | 15
        "1\n2,3\n4,5\n6"       | 21
        "1\n2\n3\n4\n5\n6\n7"  | 28
        "1,2\n3\n4,5\n6\n7\n8" | 36
    }

    def "add should properly sums n numbers with delimiters custom delimiter"() {
        expect:
        calculator.add(values) == expected

        where:
        values                 | expected
        ""                     | 0
        "//;\n1"               | 1
        "//;\n1;2"             | 3
        "//;\n1;2;3"           | 6
        "//;\n1;2;3;4"         | 10
        "//;\n1;2;3;4;5"       | 15
        "//;\n1;2;3;4;5;6"     | 21
        "//;\n1;2;3;4;5;6;7"   | 28
        "//;\n1;2;3;4;5;6;7;8" | 36
    }

    def "add should throw an exception if there was a negative number in string number"() {
        when:
        calculator.add("1,-3,-5,-6,-4")

        then:
        def a = thrown IllegalArgumentException
        a.getMessage() == "Negatives not allowed -3,-5,-6,-4"
    }


    def "add should ignore numbers higher than 1000"() {
        expect:
        calculator.add(values) == expected

        where:
        values                 | expected
        ""                     | 0
        "1,999"                | 1000
        "1,2,1231"             | 3
        "1,2,3,123918204"      | 6
        "1,2,3,4,1451"         | 10
        "1,2,3,4,5,1244"       | 15
        "1,2,3,4,5,6,9093"     | 21
        "1,2,3,4,5,6,7,94991"  | 28
        "1,2,3,4,5,6,7,8,1929" | 36
    }


    def "add should allow multiple custom delimiters in numbers string"() {
        expect:
        calculator.add(values) == expected

        where:
        values                        | expected
        ""                            | 0
        "//[%][*]\n1"                 | 1
        "//[%][*]\n1*2"               | 3
        "//[%][*]\n1%2*3"             | 6
        "//[%][*]\n1,2*3%4"           | 10
        "//[%][*]\n1,2\n3%4*5"        | 15
        "//[%][*]\n1*2,3%4*5\n6"      | 21
        "//[%][*]\n1,2,3%4*5,6\n7"    | 28
        "//[%][*]\n1*2%3*4,5,6\n7\n8" | 36
    }


    def "add should allow multiple custom delimiters with length longer than 1 in numbers string"() {
        expect:
        calculator.add(values) == expected

        where:
        values                                   | expected
        ""                                       | 0
        "//[%%][****]\n1"                        | 1
        "//[%%][****]\n1****2"                   | 3
        "//[%%][****]\n1%%2****3"                | 6
        "//[%%][****]\n1,2****3%%4"              | 10
        "//[%%][****]\n1,2\n3%%4****5"           | 15
        "//[%%][****]\n1****2,3%%4****5\n6"      | 21
        "//[%%][****]\n1,2,3%%4****5,6\n7"       | 28
        "//[%%][****]\n1****2%%3****4,5,6\n7\n8" | 36
    }

}
