package pl.rybak.stringaclculator;

import pl.rybak.stringaclculator.domain.CustomDelimiterType;
import pl.rybak.stringaclculator.utils.Utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static pl.rybak.stringaclculator.domain.CustomDelimiterType.CUSTOM_MULTIPLE_DELIMITER;
import static pl.rybak.stringaclculator.domain.CustomDelimiterType.CUSTOM_ONE_DELIMITER;

public class StringCalculator {

    public int add(String numbers) {
        List<Integer> parsed = getNumbersList(numbers);
        checkForNegativeNumbers(parsed);
        return parsed.stream()
                .reduce(0, Math::addExact);
    }

    private List<Integer> getNumbersList(String numbers) {
        return splitByCustomDelimiter(numbers, CUSTOM_ONE_DELIMITER).stream()
                .flatMap(num -> splitByCustomDelimiter(num, CUSTOM_MULTIPLE_DELIMITER).stream())
                .flatMap(num -> Arrays.stream(num.split(",")))
                .flatMap(num -> Arrays.stream(num.split("\n")))
                .filter(el -> !el.isEmpty())
                .map(Integer::parseInt)
                .filter(el -> el < 1000)
                .collect(Collectors.toList());
    }

    private void checkForNegativeNumbers(List<Integer> parsed) {
        List<String> negative = parsed.stream()
                .filter(el -> el < 0)
                .map(String::valueOf)
                .collect(Collectors.toList());
        if (!negative.isEmpty()) {
            throw new IllegalArgumentException("Negatives not allowed " + String.join(",", negative));
        }
    }

    private List<String> splitByCustomDelimiter(String numbers, CustomDelimiterType type) {
        Matcher matcher = Pattern.compile(type.getPattern()).matcher(numbers);
        if (matcher.find()) {
            numbers = numbers.replaceAll(type.getPattern(), "");
            String customDelimiter = clearDelimiter(type, matcher.group(type.getGroup()));
            return Arrays.asList(numbers.split(customDelimiter));
        }
        return Collections.singletonList(numbers);
    }

    private String clearDelimiter(CustomDelimiterType type, String customDelimiter) {
        if (type.isClear()) {
            customDelimiter = Arrays.stream(customDelimiter.split(type.getClearingPattern()))
                                .map(Utils::escape)
                                .collect(Collectors.joining("|"));
        }
        return customDelimiter;
    }
}
