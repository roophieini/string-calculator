package pl.rybak.stringaclculator.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.regex.Pattern;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Utils {

    public static String escape(String text) {
        if (text.length() > 1) {
            return Pattern.quote(text);
        }
        return "\\" + text;
    }

}
