package pl.rybak.stringaclculator.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CustomDelimiterType {

    CUSTOM_MULTIPLE_DELIMITER("\\/\\/(\\[(\\D+)\\])\\n", 2, true, "\\]\\["),
    CUSTOM_ONE_DELIMITER("\\/\\/(\\D{1})\\n", 1, false, "");

    private String pattern;
    private int group;
    private boolean clear;
    private String clearingPattern;
}
